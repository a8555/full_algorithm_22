import os
import json
import nibabel as nib
import numpy as np


def sort_slices(path, name):
    pos_dict = {}
    neg_dict = {}
    slice_dict = {}
    patients = os.listdir(path)
    for patient in patients:
    # patient = patients[0]

        patient_path = os.path.join(path, patient)
        ct_path = os.path.join(patient_path, 'CT')
        gt_path = os.path.join(patient_path, 'GT')
        gt_lung_path = os.path.join(gt_path, 'Lung')
        gt_gtv_path = os.path.join(gt_path, 'GTV')

        gt_pos = []
        gt_neg = []
        gt_slices = []
        numbering = []
        contents = os.listdir(ct_path)
        for i, _ in enumerate(contents):
            numbering.append(i)

        ct = np.zeros([512, 512, len(os.listdir(ct_path))])
        gt = np.zeros([512, 512, len(os.listdir(ct_path))])

        for layer, content in enumerate(numbering):
            ct_fname = str(content) + '.nii.gz'
            gt_fname = str(content) + '_gtv.nii.gz'
            gt_patch_gtv = nib.load(os.path.join(gt_gtv_path, gt_fname)).get_fdata()
            gt[:, :, layer] = gt_patch_gtv

        if np.max(gt) > 0:
            for layer, content in enumerate(numbering):
                ct_fname = str(content) + '.nii.gz'
                gt_fname = str(content) + '_gtv.nii.gz'
                gt_patch_gtv = nib.load(os.path.join(gt_gtv_path, gt_fname)).get_fdata()
                # ct_patch = nib.load(os.path.join(ct_path, str(layer) + '_ct.nii.gz')).get_fdata()
                # gt_patch_gtv = nib.load(os.path.join(gt_gtv_path, str(layer) + '_gtv.nii.gz')).get_fdata()
                # pet_patch = nib.load(os.path.join(pet_path, str(layer) + '_pet.nii.gz')).get_fdata()
                if np.max(gt_patch_gtv) > 0:
                    gt_slices.append(os.path.join(ct_path, ct_fname) + ',' + os.path.join(gt_gtv_path, gt_fname) + ',' + os.path.join(gt_lung_path, str(layer) + '_lung.nii.gz') + ', ' + '1')

                else:
                    gt_slices.append(os.path.join(ct_path, ct_fname) + ',' + os.path.join(gt_gtv_path, gt_fname) + ',' + os.path.join(gt_lung_path, str(layer) + '_lung.nii.gz') + ', ' + '0')

        else:
            print(f'Patient: {patient} has no GTV in image, max value: {np.max(gt)}, skipping...')

        pos_dict[patient] = gt_pos
        neg_dict[patient] = gt_neg
        slice_dict[patient] = gt_slices

        with open(name, 'w') as fp:
            json.dump(slice_dict, fp)
